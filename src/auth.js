class Auth {
  static init() {
    const self = this;

    // Create a new promise object so that we can chain operations
    // on the call to `Auth.init`. Here we use the Promise constructor
    // function, and execute code within it. In our case, we want to
    // resolve the `then` callbacks only if the accessToken is present.
    // Otherwise, we want to reject and pass a reason to the `catch`
    // callbacks. Check out User.constructor to see what happens if
    // the Promise is resolved, and what happens if the Promise is rejected.
    return new Promise((resolve, reject) => {
      const accessToken = self.getAccessToken();

      if (accessToken) {
        resolve(accessToken);
      } else {
        reject(new Error('No access token available in URL'));
      }
    });
  }

  static createAuthorizeUserButton() {
    const self = this;
    const buttonEl = document.createElement('button');
    const buttonText = document.createTextNode('Log In');
    const loginUri = self.getLoginURI();

    buttonEl.id = 'authorize-user-button';
    buttonEl.appendChild(buttonText);
    buttonEl.addEventListener('click', function(event) {
      window.open(loginUri, '_top');
    });
    document.getElementById('app-header').appendChild(buttonEl);
  }

  /**
   * Code below this line in this class
   * is specific to how we use the Spotify Web API.
   **/

  static getAccessToken() {
    // Checks the hash fragment of the window's location for a param
    // named 'access_token', and if it is present and has a value,
    // returns that value.
    //
    // Inspired by https://stackoverflow.com/a/901144
    const url = window.location.href;
    const results = new RegExp("[#&]access_token(=([^&#]*)|&|#|$)").exec(url);
    return (!results || !results[2]) ? '' : results[2];
  }

  static getLoginURI() {
    this.SPOTIFY_CLIENT_ID = '';
    this.REDIRECT_URI = '';

    if (!this.SPOTIFY_CLIENT_ID) throw new Error('Developer must set Spotify Client ID in source code.');
    if (!this.REDIRECT_URI) throw new Error('Developer must set Redirect URI in source code');

    const encodedRedirectUri = encodeURIComponent(this.REDIRECT_URI);
    const encodedScopes = encodeURIComponent('user-read-email user-top-read');

    return `https://accounts.spotify.com/authorize?` +
      `client_id=${this.SPOTIFY_CLIENT_ID}` +
      `&redirect_uri=${encodedRedirectUri}` +
      `&scope=${encodedScopes}` +
      `&response_type=token`;
  }
}
