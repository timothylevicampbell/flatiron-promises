class User {
  constructor() {
    const self = this;

    // Here, we want to call Auth.init() to evaluate the Promise that it returns
    // and either do something with the resolved value or catch the rejection.
    //
    // If the Promise returned by Auth.init() resolves, then it will pass an
    // accessToken, and we will want to set properties on the User instance.
    // The reason we use a Promise here is that we won't know whether the
    // application has an accessToken until after the call to Auth.init(), so
    // we won't be able to say whether the user is authorized until then as well.
    //
    // If authorization is unsuccessful, alert the user, log the error to the
    // console, set the flag to false, and create the UI element for the user
    // to authorize. Note that failure of authorization does not prevent the
    // user instance from being created.
    return Auth.init()
      .then(accessToken => {
        self.isAuthorized = true;
        self.request = new Request(accessToken);
        return self;
      })
      .catch(reason => {
        console.error('No access token found');
        console.error(reason);
        self.isAuthorized = false;
        Auth.createAuthorizeUserButton();
      })
      .catch(reason => {
        console.error('Could not create authorize user button');
        console.error(reason);
      })
  }

  fetchTopArtists(term) {
    // Here we are making a call to the user's request helper. Request is only a
    // utility class to prevent code duplication. It's passed an accessToken (in
    // the constructor function above), and uses that accessToken to build the
    // appropriate request headers.
    //
    // request.get is a simple wrapper around the JavaScript fetch API that
    // takes a path, appends it to a base URL, and makes a request! In our
    // implementation, I've added a quick setTimeout in order to simulate
    // network requests that take a little time.
    return this.request.get(`/me/top/artists?limit=50&time_range=${term}`);
  }

  /*========================================================================*
   * The GenreList constructor returns a promise which we expect to evaluate
   * to a GenreList object. We are using a promise here to show a pattern
   * where during the instantiation of an object, we can make an API
   * request to get some additional information. Below are four different
   * ways to deal with making multiple API requests.
   *=======================================================================*/

  fetchTopGenres_promiseRace() {
    // Promise.race takes an array of Promises and returns only the first
    // Promise that is evaluated. In this case, it could be any of the three
    // GenreList's. Then, it takes the one value and renders it.
    const promises = this.constructor.terms.map(term => new GenreList(this, term));
    return Promise.race(promises)
      .then(genreList => genreList.render());
  }

  fetchTopGenres_promiseAll() {
    // Promise.all takes an array of Promises and waits for them all to evaluate
    // before returning an array of the resolved Promises. In this case, we are
    // passing a Promise for each time range (term), and when each Promise
    // evaluates, we get a list of GenreList objects (genreLists). For each
    // genreList object, we want to render it.
    const promises = this.constructor.terms.map(term => new GenreList(this, term));
    return Promise.all(promises)
      .then(genreLists => {
        genreLists.forEach(genreList => genreList.render());
    });
  }

  fetchTopGenres_sequentially() {
    // Sometimes we want to load Promises sequentially and not wait for them
    // all to load, but still want to preserve the original order of the
    // Promises. Here we will take advantage of how Promises behave when a
    // Promise is returned within a `then` callback, and how chained
    // `then`s will always execute even after the Promise is resolved.
    //
    // We are creating a Promise that resolves immediately. (Conversely, we could
    // create Promises that reject immediately.) We are only using this Promise
    // to chain `then`s, so we don't care about the value that resolves.
    //
    // Now, for each term in the list, we reassign the emptyPromise to the
    // value that is returned from the `then` callback for this term. (Wrap your
    // head around that for a second.) This means that before executing the
    // `then` callback for the next `term`, it will wait for the previous
    // `then` callback to resolve.
    //
    // In our case, the following code is equivalent to saying:
    //  * Instantiate the GenreList for the first term
    //  * Then render the GenreList for first term
    //  * Then instantiate the GenreList for the second term
    //  * Then render the GenreList for the second term
    //  * Then instantiate the GenreList for the third term
    //  * Then render the GenreList for the third term
    //
    // Another gotcha in this code is that we aren't using the value
    // returned from the Promise. (shrug) In this example we don't need it.
    let emptyPromise = Promise.resolve(/* emptyValue */);

    this.constructor.terms.forEach(term => {
      emptyPromise = emptyPromise
        .then(emptyValue => new GenreList(this, term))
        .then(genreList => genreList.render());
    });
  }

  fetchTopGenres_inAnyOrder() {
    // I'm putting this code last, but this may be your first instinct when
    // having to deal with multiple Promises. In this code, instead of waiting
    // for any of the other Promises to resolve, we instead just create three
    // GenreLists, which will `then` render. The downside to this will be that
    // we can't control the order that the lists are rendered.
    this.constructor.terms.forEach(term => {
      new GenreList(this, term)
        .then(genreList => genreList.render());
    });
  }
}

User.terms = ['long_term', 'medium_term', 'short_term'];
