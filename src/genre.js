class Genre {
  constructor(name, weight) {
    this.name = name;
    this.weight = weight;
  }

  template() {
    return `
      <li class="genre" title="${this.weight} of my top artists are classified as ${this.name}">
        <span class="genre-name">${this.name}</span>
        <span class="genre-weight">${this.weight}</span>
      </li>
    `;
  }
}

class GenreList {
  constructor(user, term) {
    const self = this;

    // Set term (long, medium, or short) on GenreList
    self.term = term;

    // When the GenreList instance is being created, we need to get the
    // list of genre objects, which is only available through an API call
    // to the users top artists endpoint.
    //
    // Call user.fetchTopArtists with the time range (term), then
    // convert the returned Response object into a JSON object.
    // If an error occurs by this point, catch it and log a reason
    // to the console.
    //
    // Otherwise, we want to get the weighted list of genres from the
    // JSON object representing the list of top artists. Then we take
    // that array of items and set it on the instance of GenreList that
    // we are currently creating, and want to return the instance of
    // GenreList from the last call to `then`.
    //
    // If there's an error in getting the list of genres from artists
    // or setting the items onto the current instance of GenreList,
    // catch the error and log it to the console.
    return user.fetchTopArtists(term)
      .catch(reason => {
        console.error('Error loading data from Spotify Web API');
        return reason;
      })
      .then(this.constructor.getGenresFromArtists)
      .then(items => {
        if (!items.length) throw new Error('No genres in artists list.')
        self.items = items;
        return self;
      })
      .catch(reason => {
        console.error('Error creating genre list');
        console.error(reason);
      });
  }

  template() {
    return `
      <h2>${this.constructor.termTimeMap[this.term]}</h2>
      <ul>
        ${this.items.map(item => item.template()).join('')}
      </ul>
    `;
  }

  render() {
    // Create section element with inner HTML of genre list template
    // And append the new section element to the body.
    const sectionEl = document.createElement('section');
    sectionEl.innerHTML = this.template();
    document.getElementById('app-body').appendChild(sectionEl);
  }

  static getGenresFromArtists(artists) {
    // Map each artist in artists.items to their genres.
    // Reduce the array of genres to an object whose keys
    // are the genre name, and whose value is the number
    // of times that the genre appears in the list of artists.
    const genreCount = artists.items
      .map(artist => artist.genres)
      .reduce((countObj, genres) => {
        genres.forEach(genre => {
          const count = countObj[genre];
          countObj[genre] = count ? count + 1 : 1;
        });
        return countObj;
      }, {});

    // Get the names of the genres from genreCount,
    // sort them by their value descending (highest count first), only
    // keep the top 25 genres, and remove genres that only appear once.
    // Finally, create a new Genre object from each genre with their count.
    return Object.keys(genreCount)
      .sort((genreA, genreB) => genreCount[genreB] - genreCount[genreA])
      .slice(0, 25)
      .filter(genre => genreCount[genre] > 1)
      .map(genre => new Genre(genre, genreCount[genre]));
  }
}

GenreList.termTimeMap = {
  long_term: 'of all time',
  medium_term: 'in the last six months',
  short_term: 'in the last month'
}
