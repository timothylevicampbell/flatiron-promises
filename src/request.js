class Request {
  constructor(accessToken) {
    this.accessToken = accessToken;
  }

  getHeaders() {
    return {
      headers: new Headers({ 'Authorization': `Bearer ${this.accessToken}` })
    };
  }

  get(path) {
    // `fetch` returns a Response object and for our demo, we are automatically
    // converting all responses to JSON, and then making the user wait xxx
    // milliseconds to simulate network latency. 
    return fetch(`https://api.spotify.com/v1${path}`, this.getHeaders())
      .then(this.constructor.getJson)
      .then(response => {
        return new Promise((resolve, reject) => {
          setTimeout(() => {
            resolve(response);
          }, 333)
        })
      });
  }

  static getJson(response) {
    return response.json();
  }
}
